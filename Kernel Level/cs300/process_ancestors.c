/*
process_ancestors.c

Author: Shravan Gupta
Date: 12-6-20
*/

#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <linux/sched.h>
#include <linux/cred.h>

#define ANCESTOR_NAME_LEN 16

struct process_info {
    long pid;                     /* Process ID */
    char name[ANCESTOR_NAME_LEN]; /* Program name of process */
    long state;                   /* Current process state */
    long uid;                     /* User ID of process owner */
    long nvcsw;                   /* # voluntary context switches */
    long nivcsw;                  /* # involuntary context switches */
    long num_children;            /* # children process has */
    long num_siblings;            /* # sibling process has */
};

SYSCALL_DEFINE3(
    process_ancestors,                /* syscall name for macro */
    struct process_info*, info_array, /* array of process info strct */
    long, size,                       /* size of the array */
    long*, num_filled)                /* # elements written to array */

{
    long numFilled = 0;
	long numFilledCheck = 0;
    struct list_head* head;
    struct process_info processInfo;
    struct task_struct* currentTask = current;
	
	//Checking for possible errors
    if(size <= 0){
        printk("Size is <= 0\n");
        return -EINVAL;
    }
	
    if(info_array == NULL || num_filled == NULL){
        printk("Info array or num filled is Null\n");
        return -EFAULT;
    }
	
	if(copy_from_user(&numFilledCheck, num_filled, sizeof(long)) != 0){
		printk("Num_filled copying failed\n");
        return -EFAULT;
	}
	
    //Loop until numFilled is less than the size of the array
    while(numFilled < size){
		
		if(copy_from_user(&processInfo, info_array+numFilled, sizeof(struct process_info)) != 0){
			printk("Info_array copying failed\n");
			return -EFAULT;
		}

        processInfo.pid = currentTask->pid;
        memcpy(processInfo.name, currentTask->comm, ANCESTOR_NAME_LEN);        
		processInfo.pid = currentTask->pid;
        processInfo.state = currentTask->state;
        processInfo.uid = (long)((currentTask->cred)->uid.val);
        processInfo.nvcsw = currentTask->nvcsw;
        processInfo.nivcsw = currentTask->nivcsw;
		processInfo.num_children = 0;
		processInfo.num_siblings = 0;
		
		//Counting children
		list_for_each(head, &currentTask->children){
			processInfo.num_children++;
		}
		
		//Counting siblings		
		list_for_each(head, &currentTask->sibling){
			processInfo.num_siblings++;
		}
		
		// printk("info_array[%ld]", numFilled);
		// printk("pid is %ld", processInfo.pid);
		// printk("state is %ld", processInfo.state);
		// printk("uid is %ld", processInfo.uid);
		// printk("nvcsw is %ld", processInfo.nvcsw);
		// printk("nivcsw is %ld", processInfo.nivcsw);
		// printk("num_children is %ld", processInfo.num_children);
		// printk("num_siblings is %ld", processInfo.num_siblings);

        if(copy_to_user(info_array+numFilled, &processInfo, sizeof(struct process_info)) != 0){
            printk("Copy to user from processInfo to info_array failed\n");
            return -EFAULT;
        }
		
		numFilled++;
		
		//Break the loop when the parent of the process is itself
		if(currentTask->parent == currentTask){
			break;
		}
		
        currentTask = currentTask->parent;
    }
	//printk("numFilled = %ld", numFilled);
	
    if(copy_to_user(num_filled, &numFilled, sizeof(long)) != 0){
        printk("Copy to user from numFilled to num_filled failed\n");
        return -EFAULT;
    }
	
	//Success
    return 0;
}
