/*
array_stats.c

Author: Shravan Gupta
Date: 12-6-20
*/

#include <linux/kernel.h>
#include <linux/syscalls.h>

struct array_stats_s {
    long min;
    long max;
    long sum;
};

SYSCALL_DEFINE3(
    array_stats,                  /* syscall name */
    struct array_stats_s*, stats, /* where to write stats */
    long*, data,                  /* data to process */
    long, size)                   /* # values in data */

{
	long numFilled = 0;
	long currentData = 0;
	long min = 0;
	long max = 0;
	long sum = 0;
	struct array_stats_s arrayStats = {0, 0, 0};
	
	//Checking for possible errors
    if(size <= 0){
        printk("Size is <= 0\n");
        return -EINVAL;
    }
	
    if(data == NULL || stats == NULL){
        printk("Data or Stats is Null\n");
        return -EFAULT;
    }
	
	//Copy data[0] to max
    if(copy_from_user(&max, data+numFilled, sizeof(long)) != 0){
        printk("Data[0] copying failed\n");
        return -EFAULT;
    }
	
	min = max;
	
	//Loop until numFilled is less than the size of the array
    while(numFilled < size){

        if(copy_from_user(&currentData, data+numFilled, sizeof(long)) != 0){
            printk("Data copying failed\n");
            return -EFAULT;
        }

        sum += currentData;

        if(currentData < min){
            min = currentData;
        }
        if(currentData > max){
            max = currentData;
        }
		
		numFilled++;
    }
	
    arrayStats.max = max;
    arrayStats.min = min;
    arrayStats.sum = sum;
	
	// printk("Max is = %ld\n", max);
	// printk("Min is = %ld\n", min);
	// printk("Sum is = %ld\n", sum);

    if(copy_to_user(stats, &arrayStats, sizeof(arrayStats)) != 0){
        printk("Stats could not be written\n");
        return -EFAULT;
    }
	
	//Success
    return 0;
}
