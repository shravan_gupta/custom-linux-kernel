/*
process_ancestors_test.c

Author: Shravan Gupta
Date: 15-6-20
*/

#include <sys/syscall.h>
#include <linux/sched.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>

#define ANCESTOR_NAME_LEN 16
#define _PROCESS_ANCESTORS_ 550

struct process_info {
    long pid;                     /* Process ID */
    char name[ANCESTOR_NAME_LEN]; /* Program name of process */
    long state;                   /* Current process state */
    long uid;                     /* User ID of process owner */
    long nvcsw;                   /* # voluntary context switches */
    long nivcsw;                  /* # involuntary context switches */
    long num_children;            /* # children process has */
    long num_siblings;            /* # sibling process has */
};


static int test_syscall(struct process_info* info_array, long size, long *num_filled){

	printf("\nTest: ..Diving to kernel level\n");
	int result = syscall(_PROCESS_ANCESTORS_, info_array, size, num_filled);
	printf("..Rising to user level w/ result = %d\n", result);
	return result;
}

void print_info_array(struct process_info* info_array, long size){

	printf("Idx#\t PID\t\t Name\t\t State\t UID\t #VCSW\t #IVCSW\t #Child\t #Sib\n");
	
	for(long i = size-1; i >=0; i--){
		
		printf(" %ld\t %ld\t %16s\t %ld\t %ld\t %ld\t %ld\t %ld\t %ld\n", 
			i, info_array[i].pid, info_array[i].name, info_array[i].state, info_array[i].uid, info_array[i].nvcsw, 
			info_array[i].nivcsw, info_array[i].num_children, info_array[i].num_siblings);
		
		// printf(" ===========info_array[%d] =============\n", i);
		// printf("pid: %ld \n", info_array[i].pid);
		// printf("name: %s \n", info_array[i].name);
		// printf("state: %ld \n", info_array[i].state);
		// printf("uid: %ld \n", info_array[i].uid);
		// printf("nvcsw: %ld \n", info_array[i].nvcsw);
		// printf("nivcsw: %ld \n", info_array[i].nivcsw);
		// printf("child_count: %ld \n", info_array[i].num_children);
		// printf("sibling_count: %ld \n", info_array[i].num_siblings);
	}
}


int main(int argc, char *argv[]){
	
	struct process_info* info_array = NULL;
	long num_filled = 0;
	long size = 0;
	int result = 0;

	printf(" Test Case 1: size = 0 \n");
	info_array = malloc(sizeof(struct process_info));
	size = 0;
	result = test_syscall(info_array, size, &num_filled);
	free(info_array);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 1---------------\n");
	printf("\n");
	
	printf(" Test Case 2: size = -3013290 \n");
	info_array = malloc(sizeof(struct process_info));
	size = -3013290;
	result = test_syscall(info_array, size, &num_filled);
	free(info_array);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 2---------------\n");
	printf("\n");
	
	printf(" Test Case 3: info_array = NULL \n");
	info_array = malloc(sizeof(struct process_info));
	size = 10;
	result = test_syscall(NULL, size, &num_filled);
	free(info_array);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 3---------------\n");
	printf("\n");
	
	printf(" Test Case 4: num_filled = NULL \n");
	info_array = malloc(sizeof(struct process_info)*10);
	size = 10;
	result = test_syscall(info_array, size, NULL);
	free(info_array);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 4---------------\n");
	printf("\n");
	
	printf(" Test Case 5: num_filled > size \n");
	info_array = malloc(sizeof(struct process_info));
	size = 1;
	result = test_syscall(info_array, size, (long*)3);
	free(info_array);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 5---------------\n");
	printf("\n");

	printf(" Test Case 6: size greater than allocated memory \n");
	info_array = malloc(sizeof(struct process_info)*10);
	size = 1000000;
	result = test_syscall(info_array, size, NULL);
	free(info_array);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 6---------------\n");
	printf("\n");

	printf(" Test Case 7: valid input \n");
	info_array = malloc(sizeof(struct process_info)*10);
	size = 10;
	result = test_syscall(info_array, size, &num_filled);
	assert(result == 0);
	assert(num_filled != 0);
	print_info_array(info_array, num_filled);
	free(info_array);
	printf("\n");
	printf("---------------Passed 7, num_filled = %ld---------------\n", num_filled); 
	printf("\n");

    printf("****************************************************\n");
    printf("                     PASSED\n");
    printf("****************************************************\n");
	
	return 0;
}

