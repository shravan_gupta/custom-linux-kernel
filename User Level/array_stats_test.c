/*
array_stats_test.c

Author: Shravan Gupta
Date: 15-6-20
*/

#include <sys/syscall.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>

// Sys-call number:
#define _ARRAY_STATS_ 549
#define MEG (1024*1024)

struct array_stats_s {
    long min;
    long max;
    long sum;
};

static int test_syscall(struct array_stats_s* stats, long* data, long size)
{
	printf("\nTest: ..Diving to kernel level\n");
	int result = syscall(_ARRAY_STATS_, stats, data, size);
	printf("..Rising to user level w/ result = %d\n", result);
	return result;

}
void print_array_stats(struct array_stats_s* stats){

	printf("Stats: min = %ld, max = %ld, sum = %ld\n",
			stats->min, stats->max, stats->sum);
}


int main(int argc, char *argv[]){
	
	struct array_stats_s arrayStats;
	int result = 0;

	printf(" Test Case 1: size = 0 \n");
	result = test_syscall(&arrayStats, &(long){1}, 0);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 1---------------\n");
	printf("\n");
	
	printf(" Test Case 2: size = -3013290 \n");
	result = test_syscall(&arrayStats, &(long){1}, -3013290);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 2---------------\n");
	printf("\n");
	
	printf(" Test Case 3: size is bad \n");
	result = test_syscall(&arrayStats, &(long){1}, 10*MEG);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 3---------------\n");
	printf("\n");
	
	printf(" Test Case 4: arrayStats = NULL \n");
	result = test_syscall(NULL, &(long){1}, 1);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 4---------------\n");
	printf("\n");
	
	printf(" Test Case 5: data = NULL \n");
	result = test_syscall(&arrayStats, NULL, 1);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 5---------------\n");
	printf("\n");
	
	printf(" Test Case 6: bad data pointer \n");
	result = test_syscall(&arrayStats, (long*)1LL, 1);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 6---------------\n");
	printf("\n");

	printf(" Test Case 7: bad data pointer 2 \n");
	result = test_syscall(&arrayStats, (long*)123456789012345689LL, 1);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 7---------------\n");
	printf("\n");
	
	printf(" Test Case 8: arrayStats is bad \n");
	result = test_syscall((void*)1, &(long){1}, 1);
	assert(result != 0);
	printf("\n");
	printf("---------------Passed 8---------------\n");
	printf("\n");

	printf(" Test Case 9: valid input \n");
	result = test_syscall(&arrayStats, (long[]){0,-6,-2}, 3);
	print_array_stats(&arrayStats);
	printf("\n");
	printf("---------------Passed 9---------------\n"); 
	printf("\n");
	
	printf(" Test Case 10: valid input 2 \n");
	result = test_syscall(&arrayStats, (long[]){-1,2,3,-4}, 4);
	print_array_stats(&arrayStats);
	printf("\n");
	printf("---------------Passed 10---------------\n"); 
	printf("\n");

    printf("****************************************************\n");
    printf("                     PASSED\n");
    printf("****************************************************\n");
	
	return 0;
}
